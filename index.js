(function() {
    window.onload = () => {
        // your page initialization code here
        // the DOM will be available here
        let w = new MLX_Window(200, 200, document.body);
        let image = new MLX_Image(100, 100);
        let red = new MLX_RGBA(123, 0, 12, 255);
        let green = new MLX_RGBA(0, 123, 12, 255);
        let ptr = image.get_data_addr();
        let i = 0;
        while (i < 20)
        {
            ptr[(i * 4) + 0] = red.r;
            ptr[(i * 4) + 1] = red.g;
            ptr[(i * 4) + 2] = red.b;
            ptr[(i * 4) + 3] = red.a;
            i++;
        }
        w.context.font = "1em Serif";
        w.string_put(green, "s", 10, 10);
        w.put_image_to_window(image, 10, 10);
        w.string_put(red, "s", 10, 10);
        i = 0;
        while (i < 40)
        {
            w.pixel_put(green, i % image.width, i % image.width);
            i++;
        }
    }
 })();
