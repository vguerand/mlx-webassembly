var MLX_RGBA = /** @class */ (function () {
    function MLX_RGBA(r, g, b, a) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }
    MLX_RGBA.prototype.componentToHex = function (c) {
        var hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    };
    MLX_RGBA.prototype.rgbToHex = function () {
        return "#" + this.componentToHex(this.r) + this.componentToHex(this.g) + this.componentToHex(this.b);
    };
    return MLX_RGBA;
}());
var MLX_Window = /** @class */ (function () {
    function MLX_Window(x, y, parent) {
        this.width = x;
        this.height = y;
        this.image = document.createElement("canvas");
        this.image.width = this.width;
        this.image.height = this.height;
        this.context = this.image.getContext('2d');
        parent.appendChild(this.image);
    }
    MLX_Window.prototype.pixel_put = function (color, x, y) {
        var pixelImageData;
        var pixelData;
        pixelImageData = this.context.createImageData(1, 1);
        pixelData = pixelImageData.data;
        pixelData[0] = color.r;
        pixelData[1] = color.g;
        pixelData[2] = color.b;
        pixelData[3] = color.a;
        this.context.putImageData(pixelImageData, x, y);
    };
    MLX_Window.prototype.put_image_to_window = function (image, x, y) {
        var data = this.context.createImageData(image.width, image.height);
        image.get_data_addr().forEach(function (v, i) {
            data.data[i] = image.get_data_addr()[i];
        });
        this.context.putImageData(data, x, y);
    };
    MLX_Window.prototype.string_put = function (color, str, x, y) {
        this.context.fillStyle = color.rgbToHex();
        this.context.fillText(str, x, y);
    };
    return MLX_Window;
}());
var MLX_Image = /** @class */ (function () {
    function MLX_Image(x, y) {
        this.width = x;
        this.height = y;
        this.image = document.createElement("canvas");
        this.image.width = this.width;
        this.image.height = this.height;
        this.context = this.image.getContext('2d');
        this.pixelData = new Uint8ClampedArray(x * y * 4);
    }
    MLX_Image.prototype.get_data_addr = function () {
        return this.pixelData;
    };
    MLX_Image.prototype.destroy_image = function () {
        delete this.pixelData;
        delete this.context;
        this.image.remove();
    };
    return MLX_Image;
}());
