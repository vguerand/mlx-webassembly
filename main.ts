class MLX_RGBA {
    r: number;
    g: number;
    b: number;
    a: number;
    
    constructor(r: number, g: number, b: number, a: number) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    private componentToHex(c: number): string {
        let hex: string = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }

    rgbToHex(): string {
        return "#" + this.componentToHex(this.r) + this.componentToHex(this.g) + this.componentToHex(this.b);
    }
}

class MLX_Window {
    width: number;
    height: number;
    image: HTMLCanvasElement;
    context: CanvasRenderingContext2D;
    
    constructor(x: number, y: number, parent: HTMLElement) {
        this.width = x;
        this.height = y;
        this.image = document.createElement("canvas");
        this.image.width = this.width;
        this.image.height = this.height;
        this.context = this.image.getContext('2d');
        parent.appendChild(this.image);
    }

    pixel_put(color: MLX_RGBA, x: number, y: number) {
        let pixelImageData: ImageData;
        let pixelData: Uint8ClampedArray;
        pixelImageData = this.context.createImageData(1, 1);
        pixelData = pixelImageData.data;

        pixelData[0] = color.r;
        pixelData[1] = color.g;
        pixelData[2] = color.b;
        pixelData[3] = color.a;
        this.context.putImageData(pixelImageData, x, y);
    }

    put_image_to_window(image: MLX_Image, x: number, y: number) {
        let data = this.context.createImageData(image.width, image.height);
        image.get_data_addr().forEach(function(v: number, i: number) {
            data.data[i] = image.get_data_addr()[i]
        })
        this.context.putImageData(data, x, y);
    }

    string_put(color: MLX_RGBA, str: string, x: number, y: number) {
        this.context.fillStyle = color.rgbToHex();
        this.context.fillText(str, x, y);
    }
}

class MLX_Image {
    width: number;
    height: number;
    image: HTMLCanvasElement;
    context: CanvasRenderingContext2D;
    private pixelData: Uint8ClampedArray;
    
    constructor(x: number, y: number) {
        this.width = x;
        this.height = y;
        this.image = document.createElement("canvas");
        this.image.width = this.width;
        this.image.height = this.height;
        this.context = this.image.getContext('2d');
        this.pixelData = new Uint8ClampedArray(x * y * 4);
    }

    get_data_addr(): Uint8ClampedArray{
        return this.pixelData;
    }

    destroy_image() {
        delete this.pixelData;
        delete this.context;
        this.image.remove();
    }
}
